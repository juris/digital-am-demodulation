close all;
clear all;
clc;

N = 2048; % dlzka fft vo filtroch
Fs = 48000;  
fc = 16000; % nosna
fm = 800; % modulacni kmitocet

A = 0.75; % amplituda nosne
m = 0.50; %hloubka modulace

% td = 50/Fs;
td = 1;  % doba trvani
Nd = td * Fs;
n = linspace(0, Nd-1, Nd);

%% generovanie signalu
% generace nosne
c = A * cos(2*pi*fc*n/Fs+0);

% modulacny signal
M = cos(2*pi*fm*n/Fs+0);
% M = 10*x;

% rusive signaly s roznim spektrom
fdist = 250; % frekvencia rusiveho sign�lu
att = 0.1; % amplituda rusiveho sign�lu
xt = att * sawtooth(2*pi*fdist*n/Fs);
xs = att * square(2*pi*fdist*n/Fs);

% modulovane signal
y = (M*m+1).*c;
ysc = (M*m+1).*c - c; %AMSC
% modulovan i signal ruseni trojuholnikom
ytr = y + xt;
ysctr = ysc + xt; 
% modulovan i signal ruseni obdelnikom
ysr = y + xs;
yscsr = ysc + xs; 

% generovane signali pokopr
sig = [y; ysc; ytr; ysctr; ysr; yscsr ];

%% vygenerovanie testovacich suborov

% sound(y,Fs);

%% Vykreslenie spekter + AM modulovaneho signalu
for i = 1:(length(sig(:,1)))
    figure(i)
    subplot(3,1,2)
    plot(n/Fs*1000,sig(i,:))
    
    subplot(3,1,3)
    Y=fft(sig(i,:))/Nd;
    YY=[Y(Nd/2+2:Nd) Y(1:Nd/2)];
    nn=linspace(-Nd/2+1, Nd/2-1,Nd-1); 
    plot(nn/Nd*Fs/1000,(abs(YY)))
end;


%% filter pp
sig_pp = zeros(length(sig(:,1)),length(sig(1,:)));
for i = 1:(length(sig(:,1)))
    tmp_pp_filt = filter_pp(sig(i,:), N, Fs);
    sig_pp(i,:)= tmp_pp_filt;
end;


%% podvzorkovanie test
M = 3;
% len = length(y);
len = length(sig(1,:));
mask = repmat([1 0 0 ],[1 ceil(length(y)/M)]);
mask = mask(1:len);

ym = y.*mask;
% ds - downsample
sig_ds = zeros(length(sig(:,1)),length(sig(1,:)));
for i = 1:(length(sig(:,1)))
    sig_ds(i,:) = sig_pp(i,:).*mask;
end;

%% filter dp
sig_dp = zeros(length(sig(:,1)),length(sig(1,:)));
for i = 1:(length(sig(:,1)))
    tmp_dp_filt = filter_dp(sig_ds(i,:), N, Fs);
    sig_dp(i,:)= tmp_dp_filt;
    figure(i)
    subplot(3,1,1)
    plot(n/Fs*1000,sig_dp(i,:))
    
%     figure(i)
%     subplot(3,1,3)
%     hold on
%     Y=fft(sig_dp(i,:))/Nd;
%     YY=[Y(Nd/2+2:Nd) Y(1:Nd/2)];
% %    nn=linspace(-Nd/2+1, Nd/2-1,Nd-1); 
%     plot(nn/Nd*Fs,(abs(YY)))
end;

%% formatovanie
titles_mod = {...
    strcat('\bf�asov� priebeh AM sign�lu, m=',num2str(m)),...
    strcat('\bf�asov� priebeh AM SC sign�lu, m=',num2str(m)),...
    strcat('\bf�asov� priebeh AM sign�lu, m=',num2str(m),' ru�en� spektrom trojuholnikov�ho sign�lu'),...
   strcat( '\bf�asov� priebeh AM SC sign�lu, m=',num2str(m),' ru�en� spektrom trojuholnikov�ho sign�lu'),...
    strcat('\bf�asov� priebeh AM sign�lu, m=',num2str(m),' ru�en� spektrom obd�n�kv�ho sign�lu'),...
    strcat('\bf�asov� priebeh AM SC sign�lu, m=',num2str(m),' ru�en� spektrom obd�n�kv�ho sign�lu')...
};
titles_spec = {...
    strcat('\bfSpektrum AM sign�lu, m=',num2str(m)),...
    strcat('\bfSpektrum AM SC sign�lu, m=',num2str(m)),...
    strcat('\bfSpektrum AM sign�lu, m=',num2str(m),' ru�en� spektrom trojuholnikov�ho sign�lu'),...
   strcat( '\bfSpektrum AM SC sign�lu, m=',num2str(m),' ru�en� spektrom trojuholnikov�ho sign�lu'),...
    strcat('\bfSpektrum AM sign�lu, m=',num2str(m),' ru�en� spektrom obd�n�kv�ho sign�lu'),...
    strcat('\bfSpektrum AM SC sign�lu, m=',num2str(m),' ru�en� spektrom obd�n�kv�ho sign�lu')...
};
for i = 1:(length(sig(:,1)))
    figure(i)
    subplot(3,1,1)
    set(gca,'xlim',[4 10])
    h=title('\bf�asov� pr�b�h demodulovan�ho sign�lu');
    set(h,'FontName','MS Sans Serif')
    xlabel('{\itt} (ms) \rightarrow')
    ylabel('{\ity} (-) \rightarrow')
    legend('{\ity} ({\itt})')
    grid on
    subplot(3,1,2)
    set(gca,'xlim',[4 10])
    h=title(titles_mod(i));
    set(h,'FontName','MS Sans Serif')
    xlabel('{\itt} (ms) \rightarrow')
    ylabel('{\itx} (-) \rightarrow')
    legend('{\itx} ({\itt})')
    grid on
    subplot(3,1,3)
    set(gca,'xlim',[-24 24])
    h=title(titles_spec(i));
    set(h,'FontName','MS Sans Serif')
    xlabel('{{\itf}} (kHz) \rightarrow')
    ylabel('Modul (-) \rightarrow')
    legend('{\it\bfS} ({\itf})')
    grid on
end
