function spektrum_ds_abs_f(n,y,td,Fvz)

Nd=td*Fvz;

subplot(3,1,1)
stem(n/Fvz,y)
plot(n/Fvz,y)

Y=fft(y)/Nd;                                        %v�po�et DFT sign�lu y o d�lce Nd pomoc� FFT, v�sledek d�len Nd
YY=[Y(Nd/2+2:Nd) Y(1:Nd/2)];                        %�prava z�visl� prom�nn� pro zobrazen� dvoustrann�ho spektra
nn=linspace(-Nd/2+1, Nd/2-1,Nd-1);                  %�prava nez�visl� prom�nn� pro zobrazen� dvoustrann�ho spektra,

subplot(3,1,2)
stem(nn/Nd*Fvz,abs(YY));


p=10;
cc=ceil(fix((10^p*abs(YY)))/max(10^p*abs(YY)));     %vektor pro korekci chyby v�po�tu FFT a spr�vn� zobrazen� f�ze
                                                    %koeficient� fourierovy �ady, jejich� modul je roven nule    
cc_angle=cc.*angle(YY)/pi;                          %hadamard�v sou�in vektoru korekce chyby s vektorem f�ze koeficient� 
                                                    %fourierovy �ady, v�sledek d�len pi
subplot(3,1,3)
stem(nn/Nd*Fvz,cc_angle);


%%

subplot(3,1,1)
h=title('\bf�asov� pr�b�h kr�tkodob�ho diskr�tn�ho sign�lu');
set(h,'FontName','MS Sans Serif')
xlabel('{\itt} (s) \rightarrow')
ylabel('{\itx} (-) \rightarrow')
legend('{\itx} ({\itt})')
grid on

subplot(3,1,2)
h=title('\bfSpektrum kr�tkodob�ho diskr�tn�ho sign�lu');
set(h,'FontName','MS Sans Serif')
xlabel('{{\itf}} (Hz) \rightarrow')
ylabel('Modul (-) \rightarrow')
legend('{\it\bfS} ({\itf})')
grid on

subplot(3,1,3)
h=title('\bfSpektrum kr�tkodob�ho diskr�tn�ho sign�lu');
set(h,'FontName','MS Sans Serif')
xlabel('{{\itf}} (Hz) \rightarrow')
ylabel('F�ze (\pirad) \rightarrow')
legend('{\it\bfS} ({\itf})')
set(gca,'ylim',[-1 1])
grid on

