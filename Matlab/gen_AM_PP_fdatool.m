close all;
% clear all;
clc;

Fs = 48000;
N = 2048;

% G = [ 0.1042
%     0.1042
%     0.1236
%     1.0000];
SOS = [...
    1.0000   -1.3690    1.0000    1.0000    0.7482    0.9338;
    1.0000    1.9203    1.0000    1.0000    1.1765    0.9426;
    1.0000         0   -1.0000    1.0000    0.9469    0.8775...
    ];

% Prvn� sekce
b1 = SOS(1,1:3);
a1 = SOS(1,4:6);
[H1, f] = freqz( b1, a1, N, Fs);
k1 = 1/max( abs( H1));
% Druh� sekce
b2 = SOS(2,1:3);
a2 = SOS(2,4:6);
H2 = freqz( b2, a2, N, Fs);
k2 = 1/max( abs( k1.*H1.*H2));
% Druh� sekce
b3 = SOS(3,1:3);
a3 = SOS(3,4:6);
H3= freqz( b3, a3, N, Fs);
k3 = 1/max( abs( k1*k2.*H1.*H2.*H3));

%% zobrazenie koeficientov
disp(sprintf( '#define SECTIONS_PP 3'));
disp(sprintf( '// koexicienty pasmove propusti'));
disp(sprintf( 'static short fxcoef_pp[SECTIONS_PP][5] = {'));
disp(sprintf( '\t{'));
disp(sprintf( '\tFLOAT2FIXED(%+1.16f),\n', [myover(k1*b1(1:2)/2), -myover(a1(2)/2), myover(k1*b1(3)/2), -myover(a1(3)/2)]));
disp(sprintf( '\t},'));
disp(sprintf( '\t{'));
disp(sprintf( '\tFLOAT2FIXED(%+1.16f),\n', [myover(k2*b2(1:2)/2), -myover(a2(2)/2), myover(k2*b2(3)/2), -myover(a2(3)/2)]));
disp(sprintf( '\t},'));
disp(sprintf( '\t{'));
disp(sprintf( '\tFLOAT2FIXED(%+1.16f),\n', [myover(k3*b3(1:2)/2), -myover(a3(2)/2), myover(k3*b3(3)/2), -myover(a3(3)/2)]));
disp(sprintf( '\t},'));
disp(sprintf( '};\n'));


disp(sprintf( 'static float ftcoef_pp[SECTIONS_PP][5] = {'));
disp(sprintf( '\t{'));
disp(sprintf( '\t%+1.16f,\n', [(k1*b1(1:2)/2), -(a1(2)/2), (k1*b1(3)/2), -(a1(3)/2)]));
disp(sprintf( '\t},'));
disp(sprintf( '\t{'));
disp(sprintf( '\t%+1.16f,\n', [(k2*b2(1:2)/2), -(a2(2)/2), (k2*b2(3)/2), -(a2(3)/2)]));
disp(sprintf( '\t},'));
disp(sprintf( '\t{'));
disp(sprintf( '\t%+1.16f,\n', [(k3*b3(1:2)/2), -(a3(2)/2), (k3*b3(3)/2), -(a3(3)/2)]));
disp(sprintf( '\t},'));
disp(sprintf( '};\n'));

%% kvantovanie a porovnanie

b1 = (k1*b1/2);
a1 = ( a1/2);
b2 = (k2*b2/2);
a2 = ( a2/2);
b3 = (k3*b3/2);
a3 = ( a3/2);

[b, a] = series(b1, a1, b2, a2);
[b, a] = series(b, a, b3, a3);
[H, f] = freqz( b, a, N, Fs);

figure(1);
plot( f/1000, 20*log10(abs(H)),'b','LineWidth',2);
hold on

%
b1 = myover(b1);
a1 = myover(a1);
b2 = myover(b2);
a2 = myover(a2);
b3 = myover(b3);
a3 = myover(a3);

printsys(b1, a1, 'z')
printsys(b2, a2, 'z')
printsys(b3, a3, 'z')

[b, a] = series(b1, a1, b2, a2);
[b, a] = series(b, a, b3, a3);
[Hq, f] = freqz( b, a, N, Fs);

plot( f/1000, 20*log10(abs(Hq)),'r');

figure(2);
semilogx( f, (abs(H)-abs(Hq)),'LineWidth',1.5);


%% zobrazenie kvantovanych sekcii
[H1_final, f] = freqz( b1, a1, N, Fs);
H2_final = freqz( b2, a2, N, Fs);
H3_final = freqz( b3, a3, N, Fs);

figure(3);
subplot(2,3,1);
semilogx( f, 20*log10(abs(H1_final)),'LineWidth',1.5);
 
subplot(2,3,2);
semilogx( f, 20*log10(abs(H2_final)),'LineWidth',1.5);

subplot(2,3,3);
semilogx( f, 20*log10(abs(H3_final)),'LineWidth',1.5);

subplot(2,3,4);
zplane(b1,a1);
subplot(2,3,5);
zplane(b2,a2);
subplot(2,3,6);
zplane(b3,a3);



%% Foratovanie grafou
figure(1);
grid on;
set(gca,'xlim',[0 Fs/2000])
set(gca,'ylim',[-100 3])
xlabel('{\itf} (kHz) \rightarrow')
ylabel('Modul (dB) \rightarrow')
legend('{\it\bfH} ({\itf}) - Ide�lna','{\it\bfH} ({\itf}) - Kvantovan�','Location','NorthWest');
title('\bfKmito�tov� charakteristika syst�mu','FontName','MS Sans Serif')

figure(2);
grid on;
set(gca,'xlim',[20 Fs/2])
% set(gca,'ylim',[-80 3])
xlabel('{\itf} (Hz) \rightarrow')
% ylabel('Modul (dB) \rightarrow')
% legend('{\it\bfH} ({\itf})','{\it\bfH} ({\itf})','Location','SouthEast');
% title('\bfKmito�tov� charakteristika syst�mu','FontName','MS Sans Serif')
xlim = [20, Fs/2];
ylim = [-60, 20];

figure(3);
subplot(2,3,1);
grid on;
%set(gca,'XTick',[0,2.5,5,7.5,10])
set(gca,'xlim',xlim)
set(gca,'ylim',ylim)
xlabel('{\itf} (Hz) \rightarrow')
ylabel('Modul (dB) \rightarrow')
legend('{\it\bfH} ({\itf})','Location','SouthEast');
title('\bfKmito�tov� charakteristika syst�mu prvej sekcie','FontName','MS Sans Serif')

subplot(2,3,2);
grid on;
set(gca,'xlim',xlim)
set(gca,'ylim',ylim)
xlabel('{\itf} (Hz) \rightarrow')
ylabel('Modul (dB) \rightarrow')
legend('{\it\bfH} ({\itf})','Location','SouthEast');
title('Modulova kmito�tov� charakteristika druhej sekcie');

subplot(2,3,3);
grid on;
set(gca,'xlim',xlim)
set(gca,'ylim',ylim)
xlabel('{\itf} (Hz) \rightarrow')
ylabel('Modul (dB) \rightarrow')
legend('{\it\bfH} ({\itf})','Location','SouthEast');
title('Modulova kmito�tov� charakteristika tretej sekcie');

subplot(2,3,4);
xlabel('Re \{{\it\bfz}\} \rightarrow')
ylabel('j.Im \{{\it\bfz}\} \rightarrow')
legend('nuly','p�ly')
title('\bfNulov� body a p�ly syst�mu v rovin� ''z''','FontName','MS Sans Serif')
grid on

subplot(2,3,5);
xlabel('Re \{{\it\bfz}\} \rightarrow')
ylabel('j.Im \{{\it\bfz}\} \rightarrow')
legend('nuly','p�ly')
title('\bfNulov� body a p�ly syst�mu v rovin� ''z''','FontName','MS Sans Serif')
grid on

subplot(2,3,6);
xlabel('Re \{{\it\bfz}\} \rightarrow')
ylabel('j.Im \{{\it\bfz}\} \rightarrow')
legend('nuly','p�ly')
title('\bfNulov� body a p�ly syst�mu v rovin� ''z''','FontName','MS Sans Serif')
grid on

figure(1)