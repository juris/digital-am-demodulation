function [ y ] = filter_dp( x, N, Fs )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

SOS = [...
    1.0000   -0.6382    1.0000    1.0000   -1.9101    0.9148;
    1.0000   -1.6685    1.0000    1.0000   -1.9485    0.9652...
    ];

% Prvn� sekce
b1 = SOS(1,1:3);
a1 = SOS(1,4:6);
[H1, f] = freqz( b1, a1, N, Fs);
k1 = 1/max( abs( H1));
% Druh� sekce
b2 = SOS(2,1:3);
a2 = SOS(2,4:6);
H2 = freqz( b2, a2, N, Fs);
k2 = 1/max( abs( k1.*H1.*H2));

b1 = myover(k1*b1/2);
a1 = myover( a1/2);
b2 = myover(k2*b2/2);
a2 = myover( a2/2);

[b,a] = series (b1,a1,b2,a2);

%% filter
y = filter(b,a,x);

end

