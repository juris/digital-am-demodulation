function [y] = myover( x)

[p,m] = getqpm();

%ymax = mybin2dec( ['0', repmat( '1',1,p), '.', repmat( '1',1,m)]);
%ymin = mybin2dec( ['1', repmat( '0',1,p), '.', repmat( '0',1,m)]);
ymax = 2^(p)-2^(-m);
ymin = -2^(p);

y = ( mod(( x - ymin)*2^m, (ymax-ymin)*2^m)/2^m + ymin);
