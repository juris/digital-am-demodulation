function [y] = mytrunc( x)

[p,m] = getqpm();

y = floor( x* 2^(m))/2^(m);
