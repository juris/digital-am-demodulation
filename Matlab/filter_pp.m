function [ y ] = filter_pp( x ,N ,Fs)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

SOS = [...
    1.0000   -1.3690    1.0000    1.0000    0.7482    0.9338;
    1.0000    1.9203    1.0000    1.0000    1.1765    0.9426;
    1.0000         0   -1.0000    1.0000    0.9469    0.8775...
    ];

% Prvn� sekce
b1 = SOS(1,1:3);
a1 = SOS(1,4:6);
[H1, f] = freqz( b1, a1, N, Fs);
k1 = 1/max( abs( H1));
% Druh� sekce
b2 = SOS(2,1:3);
a2 = SOS(2,4:6);
H2 = freqz( b2, a2, N, Fs);
k2 = 1/max( abs( k1.*H1.*H2));
% Druh� sekce
b3 = SOS(3,1:3);
a3 = SOS(3,4:6);
H3= freqz( b3, a3, N, Fs);
k3 = 1/max( abs( k1*k2.*H1.*H2.*H3));

b1 = myover(k1*b1/2);
a1 = myover( a1/2);
b2 = myover(k2*b2/2);
a2 = myover( a2/2);
b3 = myover(k3*b3/2);
a3 = myover( a3/2);

[b,a] = series (b1,a1,b2,a2);
[b,a] = series (b,a,b3,a3);
%% filter
y = filter(b,a,x);

end

