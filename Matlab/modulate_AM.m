close all;
clear all;
clc;

N = 2048;
Fs = 48000;
fc = 16000;
fm = 800;

A = 0.75; % amplituda nosne
m = 0.50; %hloubka modulace

% td = 50/Fs;
td = 1;  % doba trvani
Nd = td * Fs;
n = linspace(0, Nd-1, Nd);

%% generovanie signalu
% generace nosne
c = A * cos(2*pi*fc*n/Fs+0);

% [ x , Fvz , bits ] = wavread ( 'HumanMusic.wav' );
% x = 2*x';
% x = x(1:(td*Fs));

% modulacny signal
M = cos(2*pi*fm*n/Fs+0);
% M = 10*x;

% rusive signaly s roznim spektrom
fdist = 250;
att = 0.1;
xt = att * sawtooth(2*pi*fdist*n/Fs);
xs = att * square(2*pi*fdist*n/Fs);

% modulovane signal
y = (M*m+1).*c;
ysc = (M*m+1).*c - c; %AMSC
% modulovan i signal ruseni trojuholnikom
ytr = y + xt;
ysctr = ysc + xt; 
% modulovan i signal ruseni obdelnikom
ysr = y + xs;
yscsr = ysc + xs; 

% generovane signali pokopr
sig = [y; ysc; ytr; ysctr; ysr; yscsr ];

%% vygenerovanie testovacich suborov
% audiowrite sposobuje clipping, treba normovat vstupn� sihn�l do spr�vneho
% rozsahu 

% filename = {'am_16_05.wav','amSC_16_05.wav',...
%             'am_T_16_05.wav','amSC_T_16_05.wav',...
%             'am_S_16_05.wav','amSC_S_16_05.wav',};
% for i = 1:(length(sig(:,1)))
% %     char(filename(i))
%     audiowrite(char(filename(i)),sig(i,:),Fs);
% end
y_aten = 1 * sig(1);
sound(y_aten,Fs);

%% Vykreslenie spekter
for i = 1:(length(sig(:,1)))
    figure(i)
    Y=fft(sig(i,:))/Nd;
    YY=[Y(Nd/2+2:Nd) Y(1:Nd/2)];
    nn=linspace(-Nd/2+1, Nd/2-1,Nd-1); 
    plot(nn/Nd*Fs,(abs(YY)))
end;

%% filter pp
sig_pp = zeros(length(sig(:,1)),length(sig(1,:)));
for i = 1:(length(sig(:,1)))
    tmp_pp_filt = filter_pp(sig(i,:), N, Fs);
    sig_pp(i,:)= tmp_pp_filt;
    figure(i)
    hold on
    Y=fft(sig_pp(i,:))/Nd;
    YY=[Y(Nd/2+2:Nd) Y(1:Nd/2)];
%    nn=linspace(-Nd/2+1, Nd/2-1,Nd-1); 
    plot(nn/Nd*Fs,(abs(YY)))
end;

%% podvzorkovanie - vytvorenie masky
M = 3;
% len = length(y);
len = length(sig(1,:));
mask = repmat([1 0 0 ],[1 ceil(length(y)/M)]);
mask = mask(1:len); % delka masky = delka signalu
ym = y.*mask;

% ds - downsample
sig_ds = zeros(length(sig(:,1)),length(sig(1,:)));
for i = 1:(length(sig(:,1)))
    sig_ds(i,:) = sig_pp(i,:).*mask;
    figure(i)
    Y=fft(sig_ds(i,:))/Nd;
    YY=[Y(Nd/2+2:Nd) Y(1:Nd/2)];
%    nn=linspace(-Nd/2+1, Nd/2-1,Nd-1); 
    plot(nn/Nd*Fs,(abs(YY)))
end;

%% filter dp
sig_dp = zeros(length(sig(:,1)),length(sig(1,:)));
for i = 1:(length(sig(:,1)))
    tmp_dp_filt = filter_dp(sig_pp(i,:), N, Fs);
    sig_dp(i,:)= tmp_dp_filt;
    figure(i)
    hold on
    Y=fft(sig_dp(i,:))/Nd;
    YY=[Y(Nd/2+2:Nd) Y(1:Nd/2)];
%    nn=linspace(-Nd/2+1, Nd/2-1,Nd-1); 
    plot(nn/Nd*Fs,(abs(YY)))
end;

%% formatovanie
for i = 1:(length(sig(:,1)))
    figure(i)
%     title(['Z�vislost {\bfTP}, Parametry: {\itt}_t_a_v = ',num2str(m),', {\itf} = ',num2str(m,4)])
    h=title(['\bfKoeficienty DF� modulovaneho diskr�tn�ho sign�lu, m=',num2str(m)]);
    set(h,'FontName','MS Sans Serif')
    xlabel('{\itf} (Hz) \rightarrow')
    ylabel('Modul (-) \rightarrow')
    legend('{\it\bfc} ({\itk})')
    grid on
end
